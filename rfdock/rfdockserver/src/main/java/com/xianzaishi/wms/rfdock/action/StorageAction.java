package com.xianzaishi.wms.rfdock.action;

import com.xianzaishi.itemcenter.client.itemsku.SkuService;
import com.xianzaishi.itemcenter.common.result.PagedResult;
import com.xianzaishi.itemcenter.common.result.Result;
import com.xianzaishi.purchasecenter.client.purchase.PurchaseService;
import com.xianzaishi.purchasecenter.client.purchase.dto.PurchaseOrderDTO;
import com.xianzaishi.purchasecenter.client.purchase.dto.PurchaseOrderDTO.PurchaseDeliveryStatusConstants;
import com.xianzaishi.purchasecenter.client.purchase.dto.PurchaseSubOrderDTO;
import com.xianzaishi.purchasecenter.client.purchase.dto.PurchaseSubOrderDTO.PurchaseSubOrderStorageTypeConstants;
import com.xianzaishi.purchasecenter.client.purchase.query.PurchaseQuery;
import com.xianzaishi.wms.common.action.ActionAdapter;
import com.xianzaishi.wms.common.action.WmsActionAdapter;
import com.xianzaishi.wms.common.exception.BizException;
import com.xianzaishi.wms.common.utils.ObjectUtils;
import com.xianzaishi.wms.common.vo.SimpleRequestVO;
import com.xianzaishi.wms.common.vo.SimpleResultVO;
import com.xianzaishi.wms.rfdock.vo.TokenVO;
import com.xianzaishi.wms.track.domain.client.itf.IStorageDomainClient;
import com.xianzaishi.wms.track.vo.StorageDetailVO;
import com.xianzaishi.wms.track.vo.StorageVO;
import com.xianzaishi.wms.track.vo.StorageVO.StorageReasonType;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

public class StorageAction extends WmsActionAdapter {
	private static final Logger logger = Logger.getLogger(StorageAction.class);
	@Autowired
	private IStorageDomainClient storageDomainClient = null;

   @Autowired
	private  PurchaseService purchaseService;

	@Autowired
	private SkuService skuService;


	public String submitStorageDetail() {
		SimpleResultVO flag = null;
		try {
			Map<String, Class> classMap = new HashMap<String, Class>();
			classMap.put("data", StorageVO.class);
			classMap.put("details", StorageDetailVO.class);
			SimpleRequestVO simpleRequestVO = formatRequest(classMap);
			TokenVO tokenVO = getTokenInfo(simpleRequestVO.getToken());
			StorageVO storageVO = (StorageVO) simpleRequestVO.getData();
			storageVO.setAgencyId(tokenVO.getAgencyID());
			storageVO.setOperate(tokenVO.getOperator());
			storageVO.setStatu(0);

			//如果是采购
			if (StorageReasonType.PUCHASE.equals(storageVO.getOpReason())) {
				flag = storageDomainClient.getStoreIdCreateAndAccount(storageVO);
				purchaseStorage(flag, storageVO);
			} else {
				flag = storageDomainClient.createAndAccount(storageVO);
			}


		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		pushResult(flag);
		return ActionAdapter.JSON;
	}

	/**
	 * 采购入库
	 * @param flag
	 * @param storageVO
	 */
	private void purchaseStorage(SimpleResultVO flag, StorageVO storageVO) {
		//本次入库id
		Long storageId = (Long) flag.getTarget();

		//要根据入库id来获取，更具客户端传递过来的可能会出现相同的数据
		SimpleResultVO<List<StorageDetailVO>> detailVOListByStorageID = storageDomainClient
				.getStorageDetailVOListByStorageID(storageId);
		if (!detailVOListByStorageID.isSuccess()) {
			throw new BizException("根据入库id：" + storageId + "查询入库明细失败");
		}

		List<StorageDetailVO> storageVODetails = detailVOListByStorageID.getTarget();
		//获取skuIds
		HashMap<Long , Long> idMap = new HashMap<>();
		for (StorageDetailVO detail:storageVODetails) {
			if (null == detail) {
				continue;
			}
			ObjectUtils.isNull(detail.getSkuId(),"商品skuId不能为空");
			//获取主键id
			Long id = detail.getId();
			Long skuId = detail.getSkuId();
			idMap.put(skuId, id);
		}

		//查询该采购单是否采购过
		PurchaseQuery query = new PurchaseQuery();
		query.setParentPurchaseId(Integer.parseInt(storageVO.getAuditorName()));
		PagedResult<List<PurchaseOrderDTO>> listPagedResult = purchaseService.queryPurchase(query);

		if (null == listPagedResult || !listPagedResult.getSuccess() || null == listPagedResult
				.getModule() || CollectionUtils.isEmpty(listPagedResult.getModule())) {
			throw new BizException("不存在该采购单，采购入库失败");
		}

		if (PurchaseDeliveryStatusConstants.STATUS_STORAGE_ALL
				.equals(listPagedResult.getModule().get(0).getDeliveryStatus())
				|| PurchaseDeliveryStatusConstants.STATUS_STORAGE_PART
				.equals(listPagedResult.getModule().get(0).getDeliveryStatus())) {

			throw new BizException("采购入库失败，该采购单已经采购入库过");
		}


		if (flag.isSuccess() && !org.springframework.util.StringUtils
				.isEmpty(storageVO.getAuditorName())) {
			//得到总采购单
			Integer purchaseId = Integer.valueOf(storageVO.getAuditorName());
			PurchaseQuery purchaseQuery = new PurchaseQuery();
			purchaseQuery.setParentPurchaseId(purchaseId);
			PagedResult<List<PurchaseOrderDTO>> queryPurchase = purchaseService
					.queryPurchase(purchaseQuery);
//			logger.error("查询走采购单信息=" + JackSonUtil.getJson(queryPurchase));
			//总采购单获取
			if (queryPurchase.getSuccess() && CollectionUtils.isNotEmpty(queryPurchase.getModule())) {

				//成功
				//获取到所有子采购单
				Result<List<PurchaseSubOrderDTO>> querySubPurchaseListByPurId = purchaseService
						.querySubPurchaseListByPurId(purchaseId);
				if (querySubPurchaseListByPurId != null && querySubPurchaseListByPurId.getSuccess()
						&& null != querySubPurchaseListByPurId.getModule() && CollectionUtils
						.isNotEmpty(querySubPurchaseListByPurId.getModule())) {
					List<PurchaseSubOrderDTO> module = querySubPurchaseListByPurId.getModule();
					//提交的采购单
					List<StorageDetailVO> details = storageVO.getDetails();

					//客户端采购入库商品种类数量
					int size = details.size();
					int sizeShould = module.size();//本该采购商品种类数量

					boolean iswarehousing = true;//默认全部入库,指的是采购种类和要求采购商品种类一样时候的每种商品采购数量是否全部入库

					for (int a = 0; a < details.size(); a++) {

						//入库信息
						StorageDetailVO storageDetailVO = details.get(a);

						//自采购单信息
						PurchaseSubOrderDTO purchaseQualityDTO = module.get(a);
						purchaseQualityDTO.setStorageId(storageId);//给自采购单设置入库id
						purchaseQualityDTO.setStorageDetailId(idMap.get(storageDetailVO.getSkuId()));
						purchaseQualityDTO.setPartTag(PurchaseSubOrderStorageTypeConstants.PURCHASE_YES);

						//入库商品总价
						String number = storageDetailVO.getNumber();//入库总数量
						Integer count = purchaseQualityDTO.getCount();//要采购箱数
						Integer unitCost = purchaseQualityDTO.getUnitCost();//每箱成本,单位分
						Integer pcProportion = purchaseQualityDTO.getPcProportion();//每箱有多少件

						BigDecimal bigDecimal = new BigDecimal(number);
						BigDecimal box = bigDecimal.divide(new BigDecimal(pcProportion).divide(new BigDecimal("1000")),3,BigDecimal.ROUND_HALF_DOWN);//入库了多少箱
						BigDecimal realPrice = box.multiply(new BigDecimal(unitCost));

						//实际入库商品总价
						logger.error(
								"真实价格=" + realPrice.intValue() + ",skuId=" + storageDetailVO.getSkuId() + ", id = "
										+ idMap.get(storageDetailVO.getSkuId()));
						purchaseQualityDTO.setSettlementPrice(realPrice.intValue());

						//采购时间
						//采购员提交数量小于实际需求的数量  入库未完成
						BigDecimal srcNumber = new BigDecimal(storageDetailVO.getNumber());
						BigDecimal targetNumber = new BigDecimal(purchaseQualityDTO.getCount())
								.multiply(new BigDecimal(purchaseQualityDTO.getPcProportion())
										.divide(new BigDecimal("1000")));
						if (srcNumber.compareTo(targetNumber) == -1) {
							iswarehousing = false;
							purchaseQualityDTO.setFlowStatus(
									PurchaseSubOrderDTO.PurchaseSubOrderFlowStatusConstants.STORAGE_STATUS_PART);
						} else {
							purchaseQualityDTO.setFlowStatus(
									PurchaseSubOrderDTO.PurchaseSubOrderFlowStatusConstants.STORAGE_STATUS_ALL);
						}
					}


					//总采购单
					PurchaseOrderDTO purchaseOrderDTO = queryPurchase.getModule().get(0);
					purchaseOrderDTO.setStorageId(storageId);//入库id

					if (iswarehousing) {
						//全部入库
						purchaseOrderDTO.setDeliveryStatus(
								PurchaseOrderDTO.PurchaseDeliveryStatusConstants.STATUS_STORAGE_ALL);
						//审核状态
						purchaseOrderDTO.setAuditingStatus(
								PurchaseOrderDTO.PurchaseOrderAuditingStatusConstants.STATUS_SUPPLIER_AUDITING_RECEIVE);
					} else {
						//部分入库
						purchaseOrderDTO.setDeliveryStatus(
								PurchaseOrderDTO.PurchaseDeliveryStatusConstants.STATUS_STORAGE_PART);
					}

					//子采购单更新成功
					for (PurchaseSubOrderDTO purchaseSubOrderDTO : module) {
						purchaseSubOrderDTO.setArriveDate(new Date());
						//更新子采购单
						if (!purchaseService.updatePurchaseSubOrder(purchaseSubOrderDTO).getModule()) {
							throw new BizException("更新自采购单："+purchaseSubOrderDTO.getPurchaseSubId()+"失败");
						}
					}

					//更新总采购单
					if (!purchaseService.updatePurchaseOrder(purchaseOrderDTO).getModule()) {
						throw new BizException("更新总采购单失败");
					}

					//部分种类商品采购入库
					if (!isPurchaseAll(module)) {
						purchaseOrderDTO.setDeliveryStatus(PurchaseDeliveryStatusConstants.STATUS_STORAGE_PART);//总采购单部分入库
						for (PurchaseSubOrderDTO dto:module) {
							if (null == dto.getPartTag()) {
								dto.setStorageId(null);
								dto.setFlowStatus(PurchaseSubOrderDTO.PurchaseSubOrderFlowStatusConstants.FLOW_STATUS_NOT_START);
								dto.setArriveDate(null);
							}
						}

//						logger.error("曲洋，要更新的总采购单=" + JackSonUtil.getJson(module));
						if (purchaseService.updatePurchaseOrder(purchaseOrderDTO).getModule()) {
							for (PurchaseSubOrderDTO purchaseSubOrderDTO : module) {
								purchaseSubOrderDTO.setArriveDate(new Date());

//								logger.error("曲洋，要更新的子采购单=" + JackSonUtil.getJson(purchaseSubOrderDTO));
								//更新子采购单
								purchaseService.updatePurchaseSubOrder(purchaseSubOrderDTO);
							}
						}
					}


				} else {
					throw new BizException("获取子采购单信息失败");
				}
			} else {
				throw new BizException("没有对应的总采购单");
			}

		}
	}

	/**
	 * 判断是否全部采购，指的是商品种类
	 * @param module
	 */
	private Boolean isPurchaseAll(List<PurchaseSubOrderDTO> module) {
		if (null == module || CollectionUtils.isEmpty(module)) {
			return false;
		}

		int count = 0;
		int size = module.size();
		for (PurchaseSubOrderDTO dto:module) {
			Short partTag = dto.getPartTag();
			if (PurchaseSubOrderStorageTypeConstants.PURCHASE_YES.equals(partTag)) {
				count = count + 1;
			}
		}

		if (size == count) {
			return true;
		}

		return false;

	}

	public String auditStorage() {
		SimpleResultVO flag = null;
		try {
			SimpleRequestVO simpleRequestVO = formatRequest();
			TokenVO tokenVO = getTokenInfo(simpleRequestVO.getToken());

			flag = storageDomainClient.auditStorage(
					Long.parseLong((String) simpleRequestVO.getData()),
					tokenVO.getOperator());
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		pushResult(flag);
		return ActionAdapter.JSON;
	}

	public IStorageDomainClient getStorageDomainClient() {
		return storageDomainClient;
	}

	public void setStorageDomainClient(IStorageDomainClient storageDomainClient) {
		this.storageDomainClient = storageDomainClient;
	}

	public PurchaseService getPurchaseService() {
		return purchaseService;
	}

	public void setPurchaseService(PurchaseService purchaseService) {
		this.purchaseService = purchaseService;
	}

}
