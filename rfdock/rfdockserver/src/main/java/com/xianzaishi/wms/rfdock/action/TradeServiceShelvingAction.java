package com.xianzaishi.wms.rfdock.action;

import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import net.sf.json.JSONObject;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.caucho.hessian.client.HessianProxyFactory;
import com.xianzaishi.wms.common.action.ActionAdapter;
import com.xianzaishi.wms.common.action.WmsActionAdapter;
import com.xianzaishi.wms.common.exception.BizException;
import com.xianzaishi.wms.common.vo.SimpleRequestVO;
import com.xianzaishi.wms.common.vo.SimpleResultVO;
import com.xianzaishi.wms.hive.domain.client.itf.IInventoryDomainClient;
import com.xianzaishi.wms.oplog.domain.client.itf.ITradeServiceShelvingLogDomainClient;
import com.xianzaishi.wms.oplog.vo.TradeServiceShelvingLogVO;
import com.xianzaishi.wms.rfdock.vo.TokenVO;
import com.xianzaishi.wms.track.domain.client.itf.ITradeServiceShelvingDomainClient;
import com.xianzaishi.wms.track.vo.OutgoingDetailVO;
import com.xianzaishi.wms.track.vo.OutgoingVO;
import com.xianzaishi.wms.track.vo.TradeServiceShelvingDetailVO;
import com.xianzaishi.wms.track.vo.TradeServiceShelvingVO;

public class TradeServiceShelvingAction extends WmsActionAdapter {
	public static final Logger logger = Logger
			.getLogger(TradeServiceShelvingAction.class);

	@Autowired
	private IInventoryDomainClient inventoryDomainClient = null;

	@Autowired
	private ITradeServiceShelvingDomainClient tradeServiceShelvingDomainClient = null;

	public String submitShelvingDetail() {
		SimpleResultVO flag = null;
		try {
			SimpleRequestVO simpleRequestVO = formatRequest(TradeServiceShelvingDetailVO.class);
			TokenVO tokenVO = getTokenInfo(simpleRequestVO.getToken());

			TradeServiceShelvingVO tradeServiceShelvingVO = new TradeServiceShelvingVO();

			tradeServiceShelvingVO.setAgencyId(tokenVO.getAgencyID());
			tradeServiceShelvingVO.setOperate(tokenVO.getOperator());
			tradeServiceShelvingVO.setStatu(0);
			tradeServiceShelvingVO.setOpTime(new Date());

			tradeServiceShelvingVO.setDetails(Arrays
					.asList((TradeServiceShelvingDetailVO[]) simpleRequestVO
							.getData()));

			flag = tradeServiceShelvingDomainClient
					.createAndAccount(tradeServiceShelvingVO);
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		pushResult(flag);
		return ActionAdapter.JSON;
	}

	/**
	 * 退换货上架，主要是数据转换，具体的业务逻辑放在hive和track里面做。
	 * 
	 * @return
	 */
	public String tradeServiceShelving() {
		SimpleResultVO<Boolean> flag = null;
		try {
			SimpleRequestVO simpleRequestVO = formatRequest(TradeServiceShelvingLogVO.class);
			TokenVO tokenVO = getTokenInfo(simpleRequestVO.getToken());

			initVOs(tokenVO,
					(TradeServiceShelvingLogVO[]) simpleRequestVO.getData());

			TradeServiceShelvingVO tradeServiceShelvingVO = new TradeServiceShelvingVO();
			List<TradeServiceShelvingDetailVO> details = new LinkedList<>();

			tradeServiceShelvingVO.setAgencyId(tokenVO.getAgencyID());
			tradeServiceShelvingVO.setOperate(tokenVO.getOperator());
			tradeServiceShelvingVO.setStatu(0);
			tradeServiceShelvingVO.setOpTime(new Date());
			tradeServiceShelvingVO.setDetails(details);

			for (int i = 0; i < ((TradeServiceShelvingLogVO[]) simpleRequestVO
					.getData()).length; i++) {
				TradeServiceShelvingDetailVO tradeServiceShelvingDetailVO = new TradeServiceShelvingDetailVO();
				tradeServiceShelvingDetailVO.setAgencyId(tokenVO.getAgencyID());
				tradeServiceShelvingDetailVO
						.setPositionId(((TradeServiceShelvingLogVO[]) simpleRequestVO
								.getData())[i].getPositionId());
				tradeServiceShelvingDetailVO
						.setShelveNoReal(((TradeServiceShelvingLogVO[]) simpleRequestVO
								.getData())[i].getShelveNo());
				tradeServiceShelvingDetailVO
						.setSkuId(((TradeServiceShelvingLogVO[]) simpleRequestVO
								.getData())[i].getSkuId());
				tradeServiceShelvingDetailVO
						.setSlottingId(((TradeServiceShelvingLogVO[]) simpleRequestVO
								.getData())[i].getSlottingId());
			}

			flag = tradeServiceShelvingDomainClient
					.createAndAccount(tradeServiceShelvingVO);
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		pushResult(flag);
		return ActionAdapter.JSON;
	}

	private void initVOs(TokenVO tokenVO,
			TradeServiceShelvingLogVO[] tradeServiceShelvingLogVOs)
			throws BizException {
		if (tradeServiceShelvingLogVOs == null
				|| tradeServiceShelvingLogVOs.length == 0) {
			throw new BizException("数据为空！");
		}

		for (int i = 0; i < tradeServiceShelvingLogVOs.length; i++) {
			initVO(tokenVO, tradeServiceShelvingLogVOs[i]);
		}

	}

	private void initVO(TokenVO tokenVO,
			TradeServiceShelvingLogVO tradeServiceShelvingLogVO)
			throws BizException {
		tradeServiceShelvingLogVO.setAgencyId(tokenVO.getAgencyID());
		tradeServiceShelvingLogVO.setOperator(tokenVO.getOperator());
	}

	public static void main(String[] args) throws Exception {
		SimpleRequestVO simpleRequestVO = new SimpleRequestVO<>();
		simpleRequestVO.setToken("123");

		List<TradeServiceShelvingLogVO> rfvos = new LinkedList<TradeServiceShelvingLogVO>();

		TradeServiceShelvingLogVO tradeServiceShelvingLogRFVO = new TradeServiceShelvingLogVO();
		tradeServiceShelvingLogRFVO.setPositionBarcode("sjflajlf");
		tradeServiceShelvingLogRFVO.setSkuBarcode("fiausfufou");
		rfvos.add(tradeServiceShelvingLogRFVO);

		tradeServiceShelvingLogRFVO = new TradeServiceShelvingLogVO();
		tradeServiceShelvingLogRFVO.setPositionBarcode("sjflajlf");
		tradeServiceShelvingLogRFVO.setSkuBarcode("fiausfufou");
		rfvos.add(tradeServiceShelvingLogRFVO);

		simpleRequestVO.setData(rfvos);

		JSONObject tmp = (JSONObject) JSONObject.fromObject(JSONObject
				.fromObject(simpleRequestVO).toString());

		Map<String, Class> classMap = new HashMap<String, Class>();

		simpleRequestVO = (SimpleRequestVO) JSONObject.toBean(tmp,
				SimpleRequestVO.class, classMap);

		String url = "http://127.0.0.1:80/tradeServiceShelvingLogService";
		HessianProxyFactory factory = new HessianProxyFactory();
		ITradeServiceShelvingLogDomainClient tradeServiceShelvingLogService = (ITradeServiceShelvingLogDomainClient) factory
				.create(ITradeServiceShelvingLogDomainClient.class, url);

	}

	public IInventoryDomainClient getInventoryDomainClient() {
		return inventoryDomainClient;
	}

	public void setInventoryDomainClient(
			IInventoryDomainClient inventoryDomainClient) {
		this.inventoryDomainClient = inventoryDomainClient;
	}

	public ITradeServiceShelvingDomainClient getTradeServiceShelvingDomainClient() {
		return tradeServiceShelvingDomainClient;
	}

	public void setTradeServiceShelvingDomainClient(
			ITradeServiceShelvingDomainClient tradeServiceShelvingDomainClient) {
		this.tradeServiceShelvingDomainClient = tradeServiceShelvingDomainClient;
	}

}
