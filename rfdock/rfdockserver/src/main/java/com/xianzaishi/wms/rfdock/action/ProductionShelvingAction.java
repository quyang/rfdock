package com.xianzaishi.wms.rfdock.action;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.wms.common.action.ActionAdapter;
import com.xianzaishi.wms.common.action.WmsActionAdapter;
import com.xianzaishi.wms.common.exception.BizException;
import com.xianzaishi.wms.common.vo.SimpleRequestVO;
import com.xianzaishi.wms.common.vo.SimpleResultVO;
import com.xianzaishi.wms.oplog.vo.ProductionShelvingLogVO;
import com.xianzaishi.wms.rfdock.vo.TokenVO;
import com.xianzaishi.wms.track.domain.client.itf.IProductDomainClient;
import com.xianzaishi.wms.track.vo.ProductDetailVO;
import com.xianzaishi.wms.track.vo.ProductVO;

public class ProductionShelvingAction extends WmsActionAdapter {
	public static final Logger logger = Logger
			.getLogger(ProductionShelvingAction.class);

	@Autowired
	private IProductDomainClient productDomainClient = null;

	public String productionShelving() {
		SimpleResultVO flag = null;
		try {
			SimpleRequestVO simpleRequestVO = formatRequest(ProductionShelvingLogVO.class);
			TokenVO tokenVO = getTokenInfo(simpleRequestVO.getToken());

			initVOs(tokenVO,
					(ProductionShelvingLogVO[]) simpleRequestVO.getData());

			ProductVO productVO = new ProductVO();
			List<ProductDetailVO> details = new LinkedList<>();

			productVO.setAgencyId(tokenVO.getAgencyID());
			productVO.setOperate(tokenVO.getOperator());
			productVO.setStatu(0);
			productVO.setOpTime(new Date());
			productVO.setDetails(details);

			for (int i = 0; i < ((ProductVO[]) simpleRequestVO.getData()).length; i++) {
				ProductDetailVO productDetailVO = new ProductDetailVO();
				productDetailVO.setAgencyId(tokenVO.getAgencyID());
				productDetailVO
						.setPositionId(((ProductionShelvingLogVO[]) simpleRequestVO
								.getData())[i].getPositionId());
				productDetailVO
						.setNumberReal(((ProductionShelvingLogVO[]) simpleRequestVO
								.getData())[i].getShelveNo());
				productDetailVO
						.setSkuId(((ProductionShelvingLogVO[]) simpleRequestVO
								.getData())[i].getSkuId());
				productDetailVO
						.setSlottingId(((ProductionShelvingLogVO[]) simpleRequestVO
								.getData())[i].getSlottingId());
			}

			flag = productDomainClient.createProductDomain(productVO);
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		pushResult(flag);
		return ActionAdapter.JSON;
	}

	private void initVOs(TokenVO tokenVO,
			ProductionShelvingLogVO[] productionShelvingLogVOs)
			throws BizException {
		if (productionShelvingLogVOs == null
				|| productionShelvingLogVOs.length == 0) {
			throw new BizException("数据为空！");
		}

		for (int i = 0; i < productionShelvingLogVOs.length; i++) {
			initVO(tokenVO, productionShelvingLogVOs[i]);
		}

	}

	private void initVO(TokenVO tokenVO,
			ProductionShelvingLogVO productionShelvingLogVO)
			throws BizException {
		productionShelvingLogVO.setAgencyId(tokenVO.getAgencyID());
		productionShelvingLogVO.setOperate(tokenVO.getOperator());
	}

	public IProductDomainClient getProductDomainClient() {
		return productDomainClient;
	}

	public void setProductDomainClient(IProductDomainClient productDomainClient) {
		this.productDomainClient = productDomainClient;
	}

}
