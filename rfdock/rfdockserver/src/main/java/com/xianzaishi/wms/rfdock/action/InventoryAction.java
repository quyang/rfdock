package com.xianzaishi.wms.rfdock.action;

import com.xianzaishi.itemcenter.common.JackSonUtil;
import com.xianzaishi.wms.common.action.ActionAdapter;
import com.xianzaishi.wms.common.action.WmsActionAdapter;
import com.xianzaishi.wms.common.exception.BizException;
import com.xianzaishi.wms.common.vo.SimpleRequestVO;
import com.xianzaishi.wms.common.vo.SimpleResultVO;
import com.xianzaishi.wms.hive.domain.client.itf.IInventory2CDomainClient;
import com.xianzaishi.wms.hive.domain.client.itf.IInventoryConfigDomainClient;
import com.xianzaishi.wms.hive.domain.client.itf.IInventoryDomainClient;
import com.xianzaishi.wms.hive.domain.client.itf.IPositionDomainClient;
import com.xianzaishi.wms.hive.vo.InventoryQueryVO;
import com.xianzaishi.wms.hive.vo.PositionDetailVO;
import com.xianzaishi.wms.hive.vo.WmsGuardBitQueryVO;
import com.xianzaishi.wms.rfdock.vo.TokenVO;
import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.List;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

public class InventoryAction extends WmsActionAdapter {
	public static final Logger logger = Logger.getLogger(InventoryAction.class);

	@Autowired
	private IInventoryConfigDomainClient inventoryConfigDomainClient = null;
	@Autowired
	private IInventory2CDomainClient inventory2CDomainClient = null;
	@Autowired
	private IInventoryDomainClient inventoryDomainClient = null;
	@Autowired
	private IPositionDomainClient positionDomainClient = null;

	public String addInventoryCoinfig() {
		SimpleResultVO flag = null;
		try {
			SimpleRequestVO simpleRequestVO = formatRequest(PositionDetailVO.class);
			TokenVO tokenVO = getTokenInfo(simpleRequestVO.getToken());
			PositionDetailVO positionDetailVO = (PositionDetailVO) simpleRequestVO
					.getData();
			positionDetailVO.setAgencyId(tokenVO.getAgencyID());
			positionDetailVO.setGuardBit(0);
			flag = inventoryConfigDomainClient
					.addPositionDetail(positionDetailVO);
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		pushResult(flag);
		return ActionAdapter.JSON;
	}

	public String delInventoryCoinfig() {
		SimpleResultVO flag = null;
		try {
			SimpleRequestVO simpleRequestVO = formatRequest(String.class);
			TokenVO tokenVO = getTokenInfo(simpleRequestVO.getToken());
			flag = inventoryConfigDomainClient.delPositionDetail(Long
					.parseLong((String) simpleRequestVO.getData()));
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		pushResult(flag);
		return ActionAdapter.JSON;
	}

	public String getInventoryCoinfigByPositionID() {
		SimpleResultVO flag = null;
		try {
			SimpleRequestVO simpleRequestVO = formatRequest(String.class);
			TokenVO tokenVO = getTokenInfo(simpleRequestVO.getToken());
			flag = inventoryConfigDomainClient.getPositionDetailByPositionID(
					tokenVO.getAgencyID(),
					Long.parseLong((String) simpleRequestVO.getData()));
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		pushResult(flag);
		return ActionAdapter.JSON;
	}

	public String getInventoryCoinfigBySkuID() {
		SimpleResultVO flag = null;
		try {
			SimpleRequestVO simpleRequestVO = formatRequest(String.class);
			TokenVO tokenVO = getTokenInfo(simpleRequestVO.getToken());
			flag = inventoryConfigDomainClient.getPositionDetailBySkuID(
					tokenVO.getAgencyID(),
					Long.parseLong((String) simpleRequestVO.getData()));
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		pushResult(flag);
		return ActionAdapter.JSON;
	}

	/**
	 * 批量获取库存信息
	 * @return
	 */
	public String batchGetInventoryBySku() {
		SimpleResultVO flag = null;
		try {
			SimpleRequestVO simpleRequestVO = formatRequest(String.class);
			TokenVO tokenVO = getTokenInfo(simpleRequestVO.getToken());

			List<Long> skuIDs = new LinkedList<Long>();

			for (Object object : (Object[]) simpleRequestVO.getData()) {
				skuIDs.add(Long.parseLong((String) object));
			}
			flag = inventory2CDomainClient.batchGetInventoryBySKUID(
					tokenVO.getAgencyID(), skuIDs);
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		pushResult(flag);
		return ActionAdapter.JSON;
	}


	/**
	 * 插入安全库存信息
	 */
	public String insettGuardInventoryInfo() {
		SimpleResultVO flag = null;
		try {
			SimpleRequestVO simpleRequestVO = formatRequest(WmsGuardBitQueryVO.class);
			LOG.error("参数结果 1 =" + JackSonUtil.getJson(simpleRequestVO));
			TokenVO tokenVO = getTokenInfo(simpleRequestVO.getToken());
			WmsGuardBitQueryVO data = (WmsGuardBitQueryVO) simpleRequestVO.getData();

			if (null == data || data.getSkuId() < 0 || null == data.getSkuId() || data.getGuardBit() == null || data.getGuardType() == null || data.getGuardType() < 0 ) {
				throw new BizException("参数异常");
			}

			flag = inventory2CDomainClient.insettGuardInventoryInfo(data);
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		pushResult(flag);
		return ActionAdapter.JSON;
	}

	/**
	 * 设置安全库存
	 */
	public String setGuardInventory() {
		SimpleResultVO flag = null;
		try {
			SimpleRequestVO simpleRequestVO = formatRequest(InventoryQueryVO.class);
			LOG.error("参数结果 2 =" + JackSonUtil.getJson(simpleRequestVO));
			TokenVO tokenVO = getTokenInfo(simpleRequestVO.getToken());
			InventoryQueryVO data = (InventoryQueryVO) simpleRequestVO.getData();
			Long skuId = data.getSkuId();
			String guardInventory = data.getGuardInventory();
			if (null == skuId || skuId < 0 || StringUtils.isEmpty(guardInventory)
					|| new BigDecimal(guardInventory.trim()).compareTo(new BigDecimal("0")) == -1) {
				throw new BizException("参数异常");
			}
			flag = inventory2CDomainClient.setGuardInventoryBySkuId(
					skuId, guardInventory);
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		pushResult(flag);
		return ActionAdapter.JSON;
	}

	public String getInventoryBySku() {
		SimpleResultVO flag = null;
		try {
			SimpleRequestVO simpleRequestVO = formatRequest(String.class);
			TokenVO tokenVO = getTokenInfo(simpleRequestVO.getToken());
			flag = inventory2CDomainClient.getInventoryBySKUID(
					tokenVO.getAgencyID(),
					Long.parseLong((String) simpleRequestVO.getData()));
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		pushResult(flag);
		return ActionAdapter.JSON;
	}

	public String getPositionBySku() {
		SimpleResultVO flag = null;
		try {
			SimpleRequestVO simpleRequestVO = formatRequest(String.class);
			TokenVO tokenVO = getTokenInfo(simpleRequestVO.getToken());
			flag = inventoryDomainClient.getInventoryBySKUID(
					tokenVO.getAgencyID(),
					Long.parseLong((String) simpleRequestVO.getData()));
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		pushResult(flag);
		return ActionAdapter.JSON;
	}

	public String getInventoryByPositionAndSku() {
		SimpleResultVO flag = null;
		try {
			SimpleRequestVO simpleRequestVO = formatRequest(InventoryQueryVO.class);
			TokenVO tokenVO = getTokenInfo(simpleRequestVO.getToken());
			InventoryQueryVO inventoryQueryVO = (InventoryQueryVO) simpleRequestVO
					.getData();
			if (inventoryQueryVO == null
					|| inventoryQueryVO.getPositionId() == null
					|| inventoryQueryVO.getSkuId() == null) {
				throw new BizException("position or sku error!");
			}
			flag = inventoryDomainClient.getInventoryByPositionAndSku(
					inventoryQueryVO.getPositionId(),
					inventoryQueryVO.getSkuId());
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		pushResult(flag);
		return ActionAdapter.JSON;
	}

	public String recommendPositionBySku() {
		SimpleResultVO flag = null;
		try {
			SimpleRequestVO simpleRequestVO = formatRequest(String.class);
			TokenVO tokenVO = getTokenInfo(simpleRequestVO.getToken());
			Long skuID = Long.parseLong((String) simpleRequestVO.getData());
			SimpleResultVO<List<PositionDetailVO>> positionDetailVOs = inventoryConfigDomainClient
					.getPositionDetailBySkuID(tokenVO.getAgencyID(), skuID);
			if (positionDetailVOs.getTarget() != null
					&& positionDetailVOs.getTarget().size() > 0) {
				for (PositionDetailVO positionDetailVO : positionDetailVOs
						.getTarget()) {
					positionDetailVO
							.setPisitionVO(positionDomainClient
									.getPositionVOByID(
											positionDetailVO.getPositionId())
									.getTarget());
				}
			}
			flag = positionDetailVOs;
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		pushResult(flag);
		return ActionAdapter.JSON;
	}

	public IInventoryConfigDomainClient getInventoryConfigDomainClient() {
		return inventoryConfigDomainClient;
	}

	public void setInventoryConfigDomainClient(
			IInventoryConfigDomainClient inventoryConfigDomainClient) {
		this.inventoryConfigDomainClient = inventoryConfigDomainClient;
	}

	public IPositionDomainClient getPositionDomainClient() {
		return positionDomainClient;
	}

	public void setPositionDomainClient(
			IPositionDomainClient positionDomainClient) {
		this.positionDomainClient = positionDomainClient;
	}

	public IInventory2CDomainClient getInventory2CDomainClient() {
		return inventory2CDomainClient;
	}

	public void setInventory2CDomainClient(
			IInventory2CDomainClient inventory2cDomainClient) {
		inventory2CDomainClient = inventory2cDomainClient;
	}

	public IInventoryDomainClient getInventoryDomainClient() {
		return inventoryDomainClient;
	}

	public void setInventoryDomainClient(
			IInventoryDomainClient inventoryDomainClient) {
		this.inventoryDomainClient = inventoryDomainClient;
	}

}
