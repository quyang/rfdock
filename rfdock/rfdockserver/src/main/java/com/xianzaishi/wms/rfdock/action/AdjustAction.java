package com.xianzaishi.wms.rfdock.action;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.wms.common.action.ActionAdapter;
import com.xianzaishi.wms.common.action.WmsActionAdapter;
import com.xianzaishi.wms.common.exception.BizException;
import com.xianzaishi.wms.common.vo.SimpleRequestVO;
import com.xianzaishi.wms.common.vo.SimpleResultVO;
import com.xianzaishi.wms.rfdock.vo.TokenVO;
import com.xianzaishi.wms.track.domain.client.itf.IAdjustDomainClient;
import com.xianzaishi.wms.track.vo.AdjustDetailVO;
import com.xianzaishi.wms.track.vo.AdjustVO;

public class AdjustAction extends WmsActionAdapter {
	private static final Logger logger = Logger.getLogger(AdjustAction.class);
	@Autowired
	private IAdjustDomainClient adjustDomainClient = null;

	public String submitAdjustDetail() {
		SimpleResultVO flag = null;
		try {
			Map<String, Class> classMap = new HashMap<String, Class>();
			classMap.put("data", AdjustVO.class);
			classMap.put("details", AdjustDetailVO.class);
			SimpleRequestVO simpleRequestVO = formatRequest(classMap);
			TokenVO tokenVO = getTokenInfo(simpleRequestVO.getToken());

			AdjustVO adjustVO = (AdjustVO) simpleRequestVO.getData();
			adjustVO.setAgencyId(tokenVO.getAgencyID());
			adjustVO.setOperate(tokenVO.getOperator());
			adjustVO.setStatu(0);
			flag = adjustDomainClient.createAndAccount(adjustVO);
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		pushResult(flag);
		return ActionAdapter.JSON;
	}

	public String auditAdjust() {
		SimpleResultVO flag = null;
		try {
			SimpleRequestVO simpleRequestVO = formatRequest();
			TokenVO tokenVO = getTokenInfo(simpleRequestVO.getToken());
			flag = adjustDomainClient.auditAdjust(
					Long.parseLong((String) simpleRequestVO.getData()),
					tokenVO.getOperator());
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		pushResult(flag);
		return ActionAdapter.JSON;
	}

	public IAdjustDomainClient getAdjustDomainClient() {
		return adjustDomainClient;
	}

	public void setAdjustDomainClient(IAdjustDomainClient adjustDomainClient) {
		this.adjustDomainClient = adjustDomainClient;
	}

}
