package com.xianzaishi.wms.rfdock.action;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.itemcenter.common.JackSonUtil;
import com.xianzaishi.wms.common.action.ActionAdapter;
import com.xianzaishi.wms.common.exception.BizException;
import com.xianzaishi.wms.common.vo.SimpleRequestVO;
import com.xianzaishi.wms.common.vo.SimpleResultVO;
import com.xianzaishi.wms.hive.domain.client.itf.IInventoryOpReasonDomainClient;

public class InventoryOpReasonAction extends ActionAdapter {

	public static final Logger logger = Logger
			.getLogger(InventoryOpReasonAction.class);

	@Autowired
	private IInventoryOpReasonDomainClient inventoryOpReasonDomainClient = null;

	public String getInventoryOpReasonByOpType() {
		SimpleResultVO flag = null;
		try {
			SimpleRequestVO simpleRequestVO = formatRequest();
			LOG.error("入库原因="+simpleRequestVO.getData()+",inventoryOpReasonDomainClient:"+inventoryOpReasonDomainClient.getClass());
			flag = inventoryOpReasonDomainClient
					.getInventoryOpReasonByOpType(Long
							.parseLong((String) simpleRequestVO.getData()));
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		pushResult(flag);
		return ActionAdapter.JSON;
	}

	public IInventoryOpReasonDomainClient getInventoryOpReasonDomainClient() {
		return inventoryOpReasonDomainClient;
	}

	public void setInventoryOpReasonDomainClient(
			IInventoryOpReasonDomainClient inventoryOpReasonDomainClient) {
		this.inventoryOpReasonDomainClient = inventoryOpReasonDomainClient;
	}
}
