package com.xianzaishi.wms.rfdock.action;

import java.util.Arrays;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.wms.common.action.ActionAdapter;
import com.xianzaishi.wms.common.action.WmsActionAdapter;
import com.xianzaishi.wms.common.exception.BizException;
import com.xianzaishi.wms.common.vo.SimpleRequestVO;
import com.xianzaishi.wms.common.vo.SimpleResultVO;
import com.xianzaishi.wms.rfdock.vo.TokenVO;
import com.xianzaishi.wms.track.domain.client.itf.IStocktakingDomainClient;
import com.xianzaishi.wms.track.vo.StocktakingDetailVO;
import com.xianzaishi.wms.track.vo.StocktakingQueryVO;
import com.xianzaishi.wms.track.vo.StocktakingVO;

public class StocktakingAction extends WmsActionAdapter {
	private static final Logger logger = Logger
			.getLogger(StocktakingAction.class);
	@Autowired
	private IStocktakingDomainClient stocktakingDomainClient = null;

	public String submitStocktakingDetail() {
		SimpleResultVO flag = null;
		try {
			SimpleRequestVO simpleRequestVO = formatRequest(StocktakingDetailVO.class);
			TokenVO tokenVO = getTokenInfo(simpleRequestVO.getToken());
			StocktakingDetailVO[] stocktakingDetails = (StocktakingDetailVO[]) simpleRequestVO
					.getData();
			if (stocktakingDetails == null
					|| stocktakingDetails[0].getStocktakingId() == null) {
				StocktakingVO stocktakingVO = getDomainVO(tokenVO,
						stocktakingDetails);
				flag = stocktakingDomainClient
						.createStocktakingDomain(stocktakingVO);
			} else {
				if (stocktakingDetails[0].getId() != null) {
					flag = stocktakingDomainClient
							.batchModifyStocktakingDetailVOs(Arrays
									.asList(stocktakingDetails));
				} else {
					flag = stocktakingDomainClient
							.batchCreateStocktakingDetailVO(Arrays
									.asList(stocktakingDetails));
				}
			}
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		pushResult(flag);
		return ActionAdapter.JSON;
	}

	public String submitStocktakingCheckDetail() {
		SimpleResultVO flag = null;
		try {

			SimpleRequestVO simpleRequestVO = formatRequest(StocktakingDetailVO.class);
			TokenVO tokenVO = getTokenInfo(simpleRequestVO.getToken());

			StocktakingDetailVO[] stocktakingDetails = (StocktakingDetailVO[]) simpleRequestVO
					.getData();

			flag = stocktakingDomainClient
					.batchModifyStocktakingDetailVOs(Arrays
							.asList(stocktakingDetails));
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		pushResult(flag);
		return ActionAdapter.JSON;
	}

	public String submitStocktakingToCheck() {
		SimpleResultVO flag = null;
		try {

			SimpleRequestVO simpleRequestVO = formatRequest();

			TokenVO tokenVO = getTokenInfo(simpleRequestVO.getToken());

			StocktakingVO stocktakingVO = new StocktakingVO();

			stocktakingVO.setAgencyId(tokenVO.getAgencyID());
			stocktakingVO.setId(Long.parseLong((String) simpleRequestVO
					.getData()));

			flag = stocktakingDomainClient
					.submitStocktakingToCheck(stocktakingVO);
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		pushResult(flag);
		return ActionAdapter.JSON;
	}

	public String submitStocktakingToAudit() {
		SimpleResultVO flag = null;
		try {

			SimpleRequestVO simpleRequestVO = formatRequest();

			TokenVO tokenVO = getTokenInfo(simpleRequestVO.getToken());

			StocktakingVO stocktakingVO = new StocktakingVO();

			stocktakingVO.setAgencyId(tokenVO.getAgencyID());
			stocktakingVO.setId(Long.parseLong((String) simpleRequestVO
					.getData()));

			flag = stocktakingDomainClient
					.submitStocktakingToAudit(stocktakingVO);
			if (flag.isSuccess() && (Boolean) flag.getTarget()) {
				stocktakingDomainClient.accounted(stocktakingVO.getId());
			}
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		pushResult(flag);
		return ActionAdapter.JSON;
	}

	public String getStocktakingInProgress() {
		SimpleResultVO flag = null;
		try {
			SimpleRequestVO simpleRequestVO = formatRequest(StocktakingQueryVO.class);
			TokenVO tokenVO = getTokenInfo(simpleRequestVO.getToken());
			StocktakingQueryVO queryVO = (StocktakingQueryVO) simpleRequestVO
					.getData();
			if (queryVO == null) {
				queryVO = new StocktakingQueryVO();
			}
			queryVO.setAgencyId(tokenVO.getAgencyID());
			queryVO.setStatu(0);
			flag = stocktakingDomainClient.queryStocktakingVOList(queryVO);
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		pushResult(flag);
		return ActionAdapter.JSON;
	}

	public String getStocktakingChecking() {
		SimpleResultVO flag = null;
		try {
			SimpleRequestVO simpleRequestVO = formatRequest(StocktakingQueryVO.class);
			TokenVO tokenVO = getTokenInfo(simpleRequestVO.getToken());
			StocktakingQueryVO queryVO = (StocktakingQueryVO) simpleRequestVO
					.getData();
			if (queryVO == null) {
				queryVO = new StocktakingQueryVO();
			}
			queryVO.setAgencyId(tokenVO.getAgencyID());
			queryVO.setStatu(1);
			flag = stocktakingDomainClient.queryStocktakingVOList(queryVO);
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		pushResult(flag);
		return ActionAdapter.JSON;
	}

	public String getStocktaking() {
		SimpleResultVO flag = null;
		try {
			SimpleRequestVO simpleRequestVO = formatRequest();
			TokenVO tokenVO = getTokenInfo(simpleRequestVO.getToken());
			flag = stocktakingDomainClient.getStocktakingDomainByID(Long
					.parseLong((String) simpleRequestVO.getData()));
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		pushResult(flag);
		return ActionAdapter.JSON;
	}

	private StocktakingVO getDomainVO(TokenVO tokenVO,
			StocktakingDetailVO[] stocktakingDetails) {
		StocktakingVO stocktakingVO = new StocktakingVO();
		stocktakingVO.setAgencyId(tokenVO.getAgencyID());
		stocktakingVO.setOperate(tokenVO.getOperator());
		stocktakingVO.setStatu(0);

		if (stocktakingDetails != null) {
			stocktakingVO.setDetails(Arrays.asList(stocktakingDetails));
		}

		return stocktakingVO;
	}

	public IStocktakingDomainClient getStocktakingDomainClient() {
		return stocktakingDomainClient;
	}

	public void setStocktakingDomainClient(
			IStocktakingDomainClient stocktakingDomainClient) {
		this.stocktakingDomainClient = stocktakingDomainClient;
	}

}
