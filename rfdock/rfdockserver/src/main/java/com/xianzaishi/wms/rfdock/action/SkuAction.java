package com.xianzaishi.wms.rfdock.action;

import org.apache.log4j.Logger;

import com.opensymphony.xwork2.ActionSupport;
import com.xianzaishi.wms.common.action.ActionAdapter;
import com.xianzaishi.wms.common.exception.BizException;
import com.xianzaishi.wms.common.vo.SimpleRequestVO;
import com.xianzaishi.wms.common.vo.SimpleResultVO;

public class SkuAction extends ActionAdapter {
	public static final Logger logger = Logger.getLogger(SkuAction.class);

	/**
	 * 根据商品编码获取其仓位信息，辅助上架。前仓场景下若无仓位信息则取仓位配置信息。
	 * 
	 * @return 已存放库存信息
	 */
	public String getSkuDetail() {
		SimpleResultVO flag = null;
		try {
			SimpleRequestVO simpleRequestVO = formatRequest(String.class);
			String skuBarcode = (String) simpleRequestVO.getData();

		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		pushResult(flag);
		return ActionAdapter.JSON;
	}
}
