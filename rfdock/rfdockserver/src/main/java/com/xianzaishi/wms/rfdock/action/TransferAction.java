package com.xianzaishi.wms.rfdock.action;

import java.util.Arrays;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.wms.common.action.ActionAdapter;
import com.xianzaishi.wms.common.action.WmsActionAdapter;
import com.xianzaishi.wms.common.exception.BizException;
import com.xianzaishi.wms.common.vo.SimpleRequestVO;
import com.xianzaishi.wms.common.vo.SimpleResultVO;
import com.xianzaishi.wms.rfdock.vo.TokenVO;
import com.xianzaishi.wms.track.domain.client.itf.ITransferDomainClient;
import com.xianzaishi.wms.track.vo.TransferDetailVO;
import com.xianzaishi.wms.track.vo.TransferVO;

public class TransferAction extends WmsActionAdapter {
	private static final Logger logger = Logger.getLogger(TransferAction.class);
	@Autowired
	private ITransferDomainClient transferDomainClient = null;

	public String submitTransferDetail() {
		SimpleResultVO flag = null;
		try {

			TransferVO transferVO = getDomainVOFromRequest();

			flag = transferDomainClient.createAndAccount(transferVO);
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		pushResult(flag);
		return ActionAdapter.JSON;
	}

	private TransferVO getDomainVOFromRequest() {
		TransferVO transferVO = null;

		SimpleRequestVO simpleRequestVO = formatRequest(TransferDetailVO.class);
		TokenVO tokenVO = getTokenInfo(simpleRequestVO.getToken());

		TransferDetailVO[] transferDetails = (TransferDetailVO[]) simpleRequestVO
				.getData();
		if (transferDetails != null && transferDetails.length > 0) {
			transferVO = new TransferVO();
			transferVO.setDetails(getTransferDetails(tokenVO, transferDetails));
			transferVO.setAgencyId(tokenVO.getAgencyID());
			transferVO.setOperate(tokenVO.getOperator());
		}

		return transferVO;
	}

	private List<TransferDetailVO> getTransferDetails(TokenVO tokenVO,
			TransferDetailVO[] transferDetailVOArray) {
		return Arrays.asList(transferDetailVOArray);
	}

	public ITransferDomainClient getTransferDomainClient() {
		return transferDomainClient;
	}

	public void setTransferDomainClient(
			ITransferDomainClient transferDomainClient) {
		this.transferDomainClient = transferDomainClient;
	}

}
