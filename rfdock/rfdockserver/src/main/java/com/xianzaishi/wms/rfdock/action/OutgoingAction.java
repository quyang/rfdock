package com.xianzaishi.wms.rfdock.action;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.wms.common.action.ActionAdapter;
import com.xianzaishi.wms.common.action.WmsActionAdapter;
import com.xianzaishi.wms.common.exception.BizException;
import com.xianzaishi.wms.common.vo.SimpleRequestVO;
import com.xianzaishi.wms.common.vo.SimpleResultVO;
import com.xianzaishi.wms.rfdock.vo.TokenVO;
import com.xianzaishi.wms.track.domain.client.itf.IOutgoingDomainClient;
import com.xianzaishi.wms.track.vo.OutgoingDetailVO;
import com.xianzaishi.wms.track.vo.OutgoingVO;

public class OutgoingAction extends WmsActionAdapter {
	private static final Logger logger = Logger.getLogger(OutgoingAction.class);
	@Autowired
	private IOutgoingDomainClient outgoingDomainClient = null;

	public String submitOutgoingDetail() {
		SimpleResultVO flag = null;
		try {
			Map<String, Class> classMap = new HashMap<String, Class>();
			classMap.put("data", OutgoingVO.class);
			classMap.put("details", OutgoingDetailVO.class);
			SimpleRequestVO simpleRequestVO = formatRequest(classMap);
			TokenVO tokenVO = getTokenInfo(simpleRequestVO.getToken());

			OutgoingVO outgoingVO = (OutgoingVO) simpleRequestVO.getData();
			outgoingVO.setAgencyId(tokenVO.getAgencyID());
			outgoingVO.setOperate(tokenVO.getOperator());
			outgoingVO.setStatu(0);
			flag = outgoingDomainClient.createAndAccount(outgoingVO);
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		pushResult(flag);
		return ActionAdapter.JSON;
	}

	public String auditOutgoing() {
		SimpleResultVO flag = null;
		try {
			SimpleRequestVO simpleRequestVO = formatRequest();
			TokenVO tokenVO = getTokenInfo(simpleRequestVO.getToken());
			flag = outgoingDomainClient.auditOutgoing(
					Long.parseLong((String) simpleRequestVO.getData()),
					tokenVO.getOperator());
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		pushResult(flag);
		return ActionAdapter.JSON;
	}

	public IOutgoingDomainClient getOutgoingDomainClient() {
		return outgoingDomainClient;
	}

	public void setOutgoingDomainClient(
			IOutgoingDomainClient outgoingDomainClient) {
		this.outgoingDomainClient = outgoingDomainClient;
	}

}
