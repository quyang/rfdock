package com.xianzaishi.wms.rfdock.action;

import java.util.Arrays;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.wms.common.action.ActionAdapter;
import com.xianzaishi.wms.common.action.WmsActionAdapter;
import com.xianzaishi.wms.common.exception.BizException;
import com.xianzaishi.wms.common.vo.SimpleRequestVO;
import com.xianzaishi.wms.common.vo.SimpleResultVO;
import com.xianzaishi.wms.rfdock.vo.TokenVO;
import com.xianzaishi.wms.track.domain.client.itf.ISpoilageDomainClient;
import com.xianzaishi.wms.track.vo.SpoilageDetailVO;
import com.xianzaishi.wms.track.vo.SpoilageVO;

public class SpoilageAction extends WmsActionAdapter {
	private static final Logger logger = Logger.getLogger(SpoilageAction.class);
	@Autowired
	private ISpoilageDomainClient spoilageDomainClient = null;

	public String submitSpoilageDetail() {
		SimpleResultVO flag = null;
		try {

			SpoilageVO spoilageVO = getDomainVOFromRequest();

			flag = spoilageDomainClient.createSpoilageDomain(spoilageVO);
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		pushResult(flag);
		return ActionAdapter.JSON;
	}

	private SpoilageVO getDomainVOFromRequest() {
		SpoilageVO spoilageVO = null;

		SimpleRequestVO simpleRequestVO = formatRequest(SpoilageDetailVO.class);
		TokenVO tokenVO = getTokenInfo(simpleRequestVO.getToken());

		SpoilageDetailVO[] spoilageDetails = (SpoilageDetailVO[]) simpleRequestVO
				.getData();
		if (spoilageDetails != null && spoilageDetails.length > 0) {
			spoilageVO = new SpoilageVO();
			spoilageVO.setDetails(getSpoilageDetails(spoilageDetails));
			spoilageVO.setAgencyId(tokenVO.getAgencyID());
			spoilageVO.setOperate(tokenVO.getOperator());
		}

		return spoilageVO;
	}

	private List<SpoilageDetailVO> getSpoilageDetails(
			SpoilageDetailVO[] spoilageDetailVOArray) {
		return Arrays.asList(spoilageDetailVOArray);
	}

	public ISpoilageDomainClient getSpoilageDomainClient() {
		return spoilageDomainClient;
	}

	public void setSpoilageDomainClient(
			ISpoilageDomainClient spoilageDomainClient) {
		this.spoilageDomainClient = spoilageDomainClient;
	}

}
