package com.xianzaishi.wms.rfdock.action;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import net.sf.json.JSONObject;

import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.springframework.beans.factory.annotation.Autowired;

import com.opensymphony.xwork2.ActionContext;
import com.xianzaishi.wms.common.action.ActionAdapter;
import com.xianzaishi.wms.common.action.WmsActionAdapter;
import com.xianzaishi.wms.common.exception.BizException;
import com.xianzaishi.wms.common.vo.SimpleRequestVO;
import com.xianzaishi.wms.common.vo.SimpleResultVO;
import com.xianzaishi.wms.rfdock.vo.TokenVO;
import com.xianzaishi.wms.track.domain.client.itf.IBookingDeliveryDomainClient;
import com.xianzaishi.wms.track.domain.client.itf.IInspectionDomainClient;
import com.xianzaishi.wms.track.domain.client.itf.IOrderStorageDomainClient;
import com.xianzaishi.wms.track.vo.BookingDeliveryQueryVO;
import com.xianzaishi.wms.track.vo.InspectionDetailVO;
import com.xianzaishi.wms.track.vo.InspectionVO;
import com.xianzaishi.wms.track.vo.OrderStorageDetailVO;
import com.xianzaishi.wms.track.vo.OrderStorageVO;

public class OrderStorageAction extends WmsActionAdapter {
	private static final Logger logger = Logger
			.getLogger(OrderStorageAction.class);
	@Autowired
	private IOrderStorageDomainClient orderStorageDomainClient = null;
	@Autowired
	private IBookingDeliveryDomainClient bookingDeliveryDomainClient = null;
	@Autowired
	private IInspectionDomainClient inspectionDomainClient = null;

	public String queryBookingDelivery() {
		SimpleResultVO flag = null;
		try {
			SimpleRequestVO simpleRequestVO = formatRequest(BookingDeliveryQueryVO.class);
			TokenVO tokenVO = getTokenInfo(simpleRequestVO.getToken());
			BookingDeliveryQueryVO queryVO = (BookingDeliveryQueryVO) simpleRequestVO
					.getData();
			if (queryVO == null) {
				queryVO = new BookingDeliveryQueryVO();
			}
			queryVO.setAgencyId(tokenVO.getAgencyID());
			queryVO.setStatu(2);
			flag = bookingDeliveryDomainClient
					.queryBookingDeliveryVOList(queryVO);
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		pushResult(flag);
		return ActionAdapter.JSON;
	}

	public String getBookingDeliveryDetails() {
		SimpleResultVO flag = null;
		try {
			SimpleRequestVO simpleRequestVO = formatRequest();
			TokenVO tokenVO = getTokenInfo(simpleRequestVO.getToken());
			flag = bookingDeliveryDomainClient
					.getBookingDeliveryDetailVOListByBookingDeliveryID(Long
							.parseLong((String) simpleRequestVO.getData()));
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		pushResult(flag);
		return ActionAdapter.JSON;
	}

	public String submitOrderStorageDetail() {
		SimpleResultVO flag = null;
		try {
			Map<String, Class> classMap = new HashMap<String, Class>();
			classMap.put("data", OrderStorageVO.class);
			classMap.put("details", OrderStorageDetailVO.class);
			ActionContext context = ActionContext.getContext();
			HttpServletRequest request = (HttpServletRequest) context
					.get(ServletActionContext.HTTP_REQUEST);
			JSONObject tmp = (JSONObject) JSONObject.fromObject(request
					.getParameter("request"));
			SimpleRequestVO<OrderStorageVO> simpleRequestVO = (SimpleRequestVO<OrderStorageVO>) JSONObject
					.toBean(tmp, OrderStorageVO.class, classMap);
			TokenVO tokenVO = getTokenInfo(simpleRequestVO.getToken());
			OrderStorageVO orderStorageVO = simpleRequestVO.getData();
			orderStorageVO.setAgencyId(tokenVO.getAgencyID());
			orderStorageVO.setOperate(tokenVO.getOperator());
			orderStorageVO.setStatu(0);
			orderStorageDomainClient.createOrderStorageDomain(orderStorageVO);
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		pushResult(flag);
		return ActionAdapter.JSON;
	}

	public String submitInspectionDetail() {
		SimpleResultVO flag = null;
		try {
			Map<String, Class> classMap = new HashMap<String, Class>();
			classMap.put("data", InspectionVO.class);
			classMap.put("details", InspectionDetailVO.class);
			ActionContext context = ActionContext.getContext();
			HttpServletRequest request = (HttpServletRequest) context
					.get(ServletActionContext.HTTP_REQUEST);
			JSONObject tmp = (JSONObject) JSONObject.fromObject(request
					.getParameter("request"));
			SimpleRequestVO<InspectionVO> simpleRequestVO = (SimpleRequestVO<InspectionVO>) JSONObject
					.toBean(tmp, InspectionVO.class, classMap);
			TokenVO tokenVO = getTokenInfo(simpleRequestVO.getToken());
			InspectionVO inspectionVO = simpleRequestVO.getData();
			inspectionVO.setAgencyId(tokenVO.getAgencyID());
			inspectionVO.setOperate(tokenVO.getOperator());
			inspectionDomainClient.createInspectionDomain(inspectionVO);
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		pushResult(flag);
		return ActionAdapter.JSON;
	}

	public IOrderStorageDomainClient getOrderStorageDomainClient() {
		return orderStorageDomainClient;
	}

	public void setOrderStorageDomainClient(
			IOrderStorageDomainClient orderStorageDomainClient) {
		this.orderStorageDomainClient = orderStorageDomainClient;
	}

	public IBookingDeliveryDomainClient getBookingDeliveryDomainClient() {
		return bookingDeliveryDomainClient;
	}

	public void setBookingDeliveryDomainClient(
			IBookingDeliveryDomainClient bookingDeliveryDomainClient) {
		this.bookingDeliveryDomainClient = bookingDeliveryDomainClient;
	}

	public IInspectionDomainClient getInspectionDomainClient() {
		return inspectionDomainClient;
	}

	public void setInspectionDomainClient(
			IInspectionDomainClient inspectionDomainClient) {
		this.inspectionDomainClient = inspectionDomainClient;
	}

}
