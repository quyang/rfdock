package com.xianzaishi.wms.rfdock.action;

import java.util.Arrays;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.xianzaishi.wms.common.action.ActionAdapter;
import com.xianzaishi.wms.common.action.WmsActionAdapter;
import com.xianzaishi.wms.common.exception.BizException;
import com.xianzaishi.wms.common.vo.SimpleRequestVO;
import com.xianzaishi.wms.common.vo.SimpleResultVO;
import com.xianzaishi.wms.rfdock.vo.TokenVO;
import com.xianzaishi.wms.track.domain.client.itf.IRequisitionsDomainClient;
import com.xianzaishi.wms.track.vo.RequisitionsDetailVO;
import com.xianzaishi.wms.track.vo.RequisitionsQueryVO;

public class RequisitionsAction extends WmsActionAdapter {
	private static final Logger logger = Logger
			.getLogger(RequisitionsAction.class);
	@Autowired
	private IRequisitionsDomainClient requisitionsDomainClient = null;

	public String queryRequisitions() {
		SimpleResultVO flag = null;
		try {
			SimpleRequestVO simpleRequestVO = formatRequest(RequisitionsQueryVO.class);
			TokenVO tokenVO = getTokenInfo(simpleRequestVO.getToken());

			RequisitionsQueryVO requisitionsQueryVO = (RequisitionsQueryVO) simpleRequestVO
					.getData();

			if (requisitionsQueryVO == null) {
				requisitionsQueryVO = new RequisitionsQueryVO();
			}

			requisitionsQueryVO.setAgencyId(tokenVO.getAgencyID());
			requisitionsQueryVO.setStatu(3);

			flag = requisitionsDomainClient
					.queryRequisitionsVOList(requisitionsQueryVO);
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		pushResult(flag);
		return ActionAdapter.JSON;
	}

	/**
	 * 拣货路线需生成，按拣货路线顺序返回给终端
	 * 
	 * @return
	 */
	public String getRequisitionsDetail() {
		SimpleResultVO flag = null;
		try {
			SimpleRequestVO simpleRequestVO = formatRequest();
			TokenVO tokenVO = getTokenInfo(simpleRequestVO.getToken());

			flag = requisitionsDomainClient.getRequisitionsDomainByID(Long
					.parseLong((String) simpleRequestVO.getData()));
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		pushResult(flag);
		return ActionAdapter.JSON;
	}

	public String submitRequisitionsDetail() {
		SimpleResultVO flag = null;
		try {
			flag = requisitionsDomainClient
					.batchModifyRequisitionsDetailVOs(initDetails());
		} catch (BizException e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildBizErrorResult(e);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			flag = SimpleResultVO.buildErrorResult();
		}
		pushResult(flag);
		return ActionAdapter.JSON;
	}

	private List<RequisitionsDetailVO> initDetails() {
		SimpleRequestVO simpleRequestVO = formatRequest(RequisitionsDetailVO.class);
		TokenVO tokenVO = getTokenInfo(simpleRequestVO.getToken());
		RequisitionsDetailVO[] requisitionsDetailArray = (RequisitionsDetailVO[]) simpleRequestVO
				.getData();
		if (requisitionsDetailArray == null
				|| requisitionsDetailArray.length == 0) {
			throw new BizException("明细为空！");
		}
		return Arrays.asList(requisitionsDetailArray);
	}

	public IRequisitionsDomainClient getRequisitionsDomainClient() {
		return requisitionsDomainClient;
	}

	public void setRequisitionsDomainClient(
			IRequisitionsDomainClient requisitionsDomainClient) {
		this.requisitionsDomainClient = requisitionsDomainClient;
	}
}
