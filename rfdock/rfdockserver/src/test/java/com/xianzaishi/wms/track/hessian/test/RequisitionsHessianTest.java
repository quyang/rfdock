package com.xianzaishi.wms.track.hessian.test;

import java.util.LinkedList;
import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

import com.xianzaishi.wms.base.test.BaseTest;
import com.xianzaishi.wms.track.domain.client.itf.IRequisitionsDomainClient;
import com.xianzaishi.wms.track.vo.RequisitionsDetailVO;

@ContextConfiguration(locations = "classpath:spring/ac-hessian-track.xml")
public class RequisitionsHessianTest extends BaseTest {

	@Autowired
	private IRequisitionsDomainClient requisitionsDomainClient = null;

	@Test
	public void addRequisitionsDetailTest() {
		RequisitionsDetailVO requisitionsDetailVO = new RequisitionsDetailVO();
		requisitionsDetailVO.setSkuId(1l);
		requisitionsDetailVO.setPositionId(1l);
		requisitionsDetailVO.setNumberReal(1);
		requisitionsDomainClient
				.createRequisitionsDetailVO(requisitionsDetailVO);
	}

	@Test
	public void batchAddRequisitionsDetailTest() {
		List list = new LinkedList();
		for (int i = 0; i < 5; i++) {
			RequisitionsDetailVO requisitionsDetailVO = new RequisitionsDetailVO();
			requisitionsDetailVO.setSkuId(1l);
			requisitionsDetailVO.setPositionId(1l);
			requisitionsDetailVO.setNumberReal(1);
			list.add(requisitionsDetailVO);
		}
		requisitionsDomainClient.batchCreateRequisitionsDetailVO(list);
	}

	@Test
	public void updateRequisitionsDetailTest() {
		RequisitionsDetailVO requisitionsDetailVO = new RequisitionsDetailVO();
		requisitionsDetailVO.setId(1l);
		requisitionsDetailVO.setSkuId(2l);
		requisitionsDetailVO.setPositionId(1l);
		requisitionsDetailVO.setNumberReal(1);
		requisitionsDomainClient
				.modifyRequisitionsDetailVO(requisitionsDetailVO);
	}

	@Test
	public void batchUpdateRequisitionsDetailTest() {
		List list = new LinkedList();
		for (Long i = 1l; i < 5; i++) {
			Long j = i + 101;
			RequisitionsDetailVO requisitionsDetailVO = new RequisitionsDetailVO();
			requisitionsDetailVO.setId(i);
			requisitionsDetailVO.setPositionBarcode(j.toString());
			requisitionsDetailVO.setNumberReal(1);
			requisitionsDetailVO.setSkuId(j);
			requisitionsDetailVO.setPositionId(j);
			requisitionsDetailVO.setRequisitionsId(i);
			list.add(requisitionsDetailVO);
		}
		requisitionsDomainClient.batchModifyRequisitionsDetailVOs(list);
	}

	public IRequisitionsDomainClient getRequisitionsDomainClient() {
		return requisitionsDomainClient;
	}

	public void setRequisitionsDomainClient(
			IRequisitionsDomainClient requisitionsDomainClient) {
		this.requisitionsDomainClient = requisitionsDomainClient;
	}

}
